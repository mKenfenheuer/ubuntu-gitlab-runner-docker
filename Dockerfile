FROM ubuntu

WORKDIR /

RUN apt update 
RUN apt install -y curl
RUN curl -sSL https://get.docker.com/ 39 | sh 
RUN curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | bash
RUN apt install -y gitlab-runner

COPY supervisord /init
RUN chmod +x /init
RUN chmod 755 /init

CMD ["/init"]